<?php

    require "../lib/minpacker/app.php";

    header_remove();
    $etc_css_files = array(
            "basic.css",
            "popup.css",
            "header.css",
            "cmsmenu.css",
            "cmsview.css",
            "cmsform.css",
            "cmsprofile.css",
            "cmslisttable.css",
            "home.css"
        );
    header("content-type: text/css");
    echo Minpacker::css($etc_css_files);
    exit();

?>