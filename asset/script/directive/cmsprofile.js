(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    (function () {

        _allot("cmsprofile-directive", _componentDirective);
        _allot("cmsprofile-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/cmsprofile.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    methods: {
                            onModifyClick: __app.onModifyClick,
                            onModifyCancel: __app.onModifyCancel,
                            onUpdateClick: _onUpdateClick,
                            onEmailVerify: _onEmailVerify,
                            onEmailSend: _onEmailSend,
                            onPhoneVerify: _onPhoneVerify,
                            onPhoneSend: _onPhoneSend
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    GENDER_DICTIONARY: {
                            "male": "男性",
                            "female": "女性",
                            "others": "其他"
                        }
                };
        }

    }

    function _onUpdateClick() {

        if ( _confirmForm() ) __claim("put", "user/{id}/profile", {
                    "{id}": __app.tokenUser.id,
                    "first_name": __app.tokenUser.firstName,
                    "last_name": __app.tokenUser.lastName,
                    "gender": __app.tokenUser.gender,
                    "birth": __app.tokenUser.birth,
                    "address": __app.tokenUser.address
                }, _onSuccess, _onError);

        function _onSuccess() {
            __popup.alert("更新成功", _onConfirm);
        }

        function _onError(_response) {
            _response = _response || {} ;
            if ( _response.comment === "updateData empty" ) _onSuccess();
            else if ( _response.comment === "birth error" ) __popup.alert("生日有誤");
            else __popup.alert("更新有誤");
        }

        function _onConfirm() {
            window.location.reload();
        }

        function _confirmForm() {
            switch ( true ) {
                case !(__app.tokenUser.firstName):
                case !(__app.tokenUser.lastName):
                    return __popup.alert("姓名有誤");
            }
            return true;
        }

    }

    function _onEmailVerify() {

        __popup.prompt("輸入信箱驗證碼", null, _onConfirm);

        function _onConfirm(_value) {
            __popup.loading();
            __claim("put", "user/{id}/email/verify", {
                    "{id}": __app.tokenUser.id,
                    "check_code": _value
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __popup.alert("驗證成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __popup.alert("驗證碼有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onEmailSend() {

        __popup.confirm("重發信箱驗證信？", _onConfirm);

        function _onConfirm() {
            __popup.loading();
            __claim("get", "user/{id}/email/notify", {
                    "{id}": __app.tokenUser.id
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __popup.alert("發信成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __popup.alert("發信有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onPhoneVerify() {

        __popup.prompt("輸入手機驗證碼", null, _onConfirm);

        function _onConfirm(_value) {
            __popup.loading();
            __claim("put", "user/{id}/mobile/verify", {
                    "{id}": __app.tokenUser.id,
                    "check_code": _value
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __popup.alert("驗證成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __popup.alert("驗證碼有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onPhoneSend() {

        __popup.confirm("重發手機驗證信？", _onConfirm);

        function _onConfirm() {
            __popup.loading();
            __claim("get", "user/{id}/mobile/notify", {
                    "{id}": __app.tokenUser.id
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __popup.alert("發信成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __popup.alert("發信有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

}(window.allot));