(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsview-directive", _componentDirective);
        _allot("cmsview-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/cmsview.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    methods: {
                        },
                    components: {
                            "view-cmsprofile": _allot("cmsprofile-directive"),
                            "view-cmsmemberlist": _allot("cmsmemberlist-directive"),
                            "view-cmsmember": _allot("cmsmember-directive")
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {
                };
        }

        function _procedureMethods() {
            return {
                    onModifyClick: _onModifyClick,
                    onModifyCancel: _onModifyCancel
                };
        }

    }

    function _onModifyClick() {

        __app.cmsmodify = !(__app.cmsmodify);

    }

    function _onModifyCancel() {

        window.location.reload();

    }

}(window.allot));