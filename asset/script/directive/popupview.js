(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    (function () {

        _allot("popup-directive", _componentPopupView);
        _allot("popup-directive-mixin", _definedData());

    }());

    function _componentPopupView(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/popup.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    methods: {
                            onPopupClick: _onPopupClick,
                            onPopupLoginSubmit: _onPopupLoginSubmit,
                            onPopupLoginConfirm: _onPopupLoginConfirm,
                            onPopupSocialoginClick: _onPopupSocialoginClick,
                            onPopupRegisterConfirm: _onPopupRegisterConfirm,
                            onPopupRegisterCancel: _onPopupRegisterCancel,
                            onPopupPromptConfirm: _onPopupPromptConfirm,
                            onPopupPictureConfirm: _onPopupPictureConfirm,
                            onPopupConfirmConfirm: _onPopupConfirmConfirm,
                            onPopupAlertConfirm: _onPopupAlertConfirm,
                            onGotoRegister: _onGotoRegister
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    popupstats: [],
                    popupPromptMessage: "",
                    popupPromptValue: "",
                    popupConfirmMessage: "",
                    popupPictureURL: "",
                    popupAlertMessage: "",
                    loginAccount: "",
                    loginPassword: "",
                    registerAccount: "",
                    registerPassword: "",
                    registerRepassword: "",
                    registerFirstname: "",
                    registerLastname: "",
                    registerPhone: "",
                    registerAddress: "",
                    socialoginType: "",
                    socialoginID: "",
                    socialoginToken: ""
                };
        }

    }

    function _onPopupClick(_event) {

        switch ( true ) {
            case /(^|\s)cancel(\s|$)/.test(_event.target.className):
            __popup.close();
        }

    }

    function _onPopupLoginSubmit(_event) {

        _event.preventDefault();
        _onPopupLoginConfirm();

    }

    function _onPopupLoginConfirm() {

        __popup.loading();
        __claim("POST", "login", {
                account: __app.loginAccount,
                password: __app.loginPassword
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.lifetoken = _response.token;
            __cookie.addItem("LIFETOKEN", __app.lifetoken);
            __popup.methods.popupLoginConfirm();
            __popup.close("login");
            window.location.reload();
        }

        function _onError(_response) {
            if ( _response.comment === "user status error" ) {
                __app.lifetoken = ( _response.data || {} ).token;
                __cookie.addItem("LIFETOKEN", __app.lifetoken);
                __popup.alert("帳號尚未驗證完成", _gotoVerify);
            } else {
                __app.loginPassword = "";
                __popup.alert("帳號與密碼有誤");
            }
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _gotoVerify() {
            window.location.href = "/cms/";
        }

    }

    function _onPopupSocialoginClick(_type) {

        var _isGoogle = ( _type === "google" );
        var _isFacebook = ( _type === "facebook" );
        if ( _isGoogle ) _allot("socialogin").signinGoogle();
        if ( _isFacebook ) _allot("socialogin").signinFacebook();

    }

    function _onPopupRegisterConfirm() {

        var _requestData = {
                account: __app.registerAccount,
                first_name: __app.registerFirstname,
                last_name: __app.registerLastname,
                mobile_country_code: "886",
                mobile_phone: _getInspectedPhone(__app.registerPhone),
                address: __app.registerAddress
            };
        _requestData.type = __app.socialoginType || "native" ;
        if ( __app.socialoginID && __app.socialoginToken ) {
            _requestData.sns_id = __app.socialoginID;
            _requestData.sns_token = __app.socialoginToken;
        } else {
            _requestData.password = __app.registerPassword;
            _requestData.password_repeat = __app.registerRepassword;
        }
        __popup.loading();
        __claim("POST", "register", _requestData, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.lifetoken = _response.token;
            __cookie.addItem("LIFETOKEN", __app.lifetoken);
            __popup.methods.popupRegisterConfirm();
            __popup.alert("帳號註冊成功請進行驗證", _gotoVerify);
        }

        function _onError() {
            __popup.alert("資料有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _getInspectedPhone(_phone) {
            return _phone.replace(/^0/, "");
        }

        function _gotoVerify() {
            window.location.href = "/cms/";
        }

    }

    function _onPopupRegisterCancel() {

        __popup.methods.popupRegisterCancel();

    }

    function _onPopupPromptConfirm() {

        __popup.methods.popupPromptConfirm();

    }

    function _onPopupPictureConfirm() {

        __popup.methods.popupPictureConfirm();

    }

    function _onPopupConfirmConfirm() {

        __popup.methods.popupConfirmConfirm();

    }

    function _onPopupAlertConfirm() {

        __popup.methods.popupAlertConfirm();

    }

    function _onGotoRegister() {

        __popup.register();

    }

}(window.allot));