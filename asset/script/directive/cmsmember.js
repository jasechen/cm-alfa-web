(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    (function () {

        _allot("cmsmember-directive", _componentDirective);
        _allot("cmsmember-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/cmsmember.html", null, _onSuccess);
        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                            onModifyClick: __app.onModifyClick,
                            onModifyCancel: __app.onModifyCancel,
                            onUpdateClick: _onUpdateClick,
                            onDeleteClick: _onDeleteClick
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    member: {}
                };
        }

    }

    function _mounted() {

        __popup.loading();
        __claim("GET", "user/{id}", {
                "{id}": __app.slug
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            var _responseMember = _response.user || {} ;
            var _responseProfile = _responseMember.profile;
            var _memberDatum = {};
            _memberDatum.name = _responseProfile.last_name + _responseProfile.first_name ;
            _memberDatum.firstName = _responseProfile.first_name;
            _memberDatum.lastName = _responseProfile.last_name;
            _memberDatum.email = _responseProfile.email;
            _memberDatum.phone = _allot("command").getFullPhone(_responseProfile.mobile_phone);
            _memberDatum.gender = _responseProfile.gender;
            _memberDatum.birth = _responseProfile.birth;
            _memberDatum.address = _responseProfile.address;
            _memberDatum.status = _responseMember.status;
            _memberDatum.rolelist = _allot("command").getRoleList(_responseMember.roles);
            __app.member = _memberDatum;
        }

        function _onFinish() {
            __popup.close("loading");
        }

    }

    function _onUpdateClick() {

        if ( _confirmFormEmpty() ) return _onError();
        __popup.loading();
        __claim("put", "user/{id}/profile", {
                "{id}": __app.slug,
                "first_name": __app.member.firstName,
                "last_name": __app.member.lastName,
                "gender": __app.member.gender,
                "birth": __app.member.birth,
                "address": __app.member.address
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __popup.alert("更新成功", _onConfirm);
        }

        function _onError(_response) {
            _response = _response || {} ;
            if ( _response.comment === "updateData empty" ) _onSuccess();
            else __popup.alert("更新有誤");
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _onConfirm() {
            window.location.reload();
        }

        function _confirmFormEmpty() {
            if ( !(__app.member.firstName) ) return true;
            if ( !(__app.member.lastName) ) return true;
        }

    }

    function _onDeleteClick() {

        __popup.confirm("確認刪除", _onDeleteConfirm);

        function _onDeleteConfirm() {
            __popup.close("confirm");
            __popup.loading();
            __claim("delete", "user/{id}", {
                    "{id}": __app.slug
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __popup.alert("刪除成功", _onSuccessConfirm);
        }

        function _onError() {
            __popup.alert("刪除失敗");
        }

        function _onSuccessConfirm() {
            __popup.close("alert");
            window.location.hash = "#/memberlist";
        }

        function _onFinish() {
            __popup.close("loading");
        }

    }

}(window.allot));