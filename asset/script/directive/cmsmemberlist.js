(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    (function () {

        _allot("cmsmemberlist-directive", _componentDirective);
        _allot("cmsmemberlist-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/cmsmemberlist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    memberlist: []
                };
        }

    }

    function _mounted() {

        __popup.loading();
        __claim("GET", "user/list/all", {
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.memberlist = _getInspected(_response.users);
        }

        function _onFinish() {
            __popup.close("loading");
        }

        function _getInspected(_list) {
            var _index = _list.length;
            var _item;
            while ( _index-- ) {
                _item = _list[_index];
                if ( _item.status === "delete" ) _list.splice(_index, 1);
                if ( _confirmAdmin(_item.roles) ) _item.disabled = true;
                if ( _item.id === __app.tokenUser.id ) _item.disabled = true;
            }
            return _list;
        }

        function _confirmAdmin(_roleList) {
            var _index = _roleList.length;
            while ( _index-- ) {
                if ( ( _roleList[_index] || {} ).code === "admin" ) return true;
            }
        }

    }

}(window.allot));