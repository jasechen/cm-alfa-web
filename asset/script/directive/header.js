(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    (function () {

        _allot("header-directive", _componentDirective);
        _allot("header-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/asset/template/header.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    methods: {
                            onLoginClick: __app.onLoginClick,
                            onLogoutClick: _onLogoutClick
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                };
        }

    }

    function _onLogoutClick() {


        __popup.confirm("確認要登出？", _onConfirm);

        function _onConfirm() {
            __popup.close("confirm");
            __popup.loading();
            __claim("POST", "logout", null, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __cookie.removeItem("LIFETOKEN");
            if ( /\/cms\//.test(window.location.href) ) window.location.href = "/";
            else window.location.reload();
        }

        function _onError() {
            __popup.alert("錯誤發生");
        }

        function _onFinish() {
            __popup.close("loading");
        }

    }

}(window.allot));