(function (_allot, Vue) {

    "use strict";

    var __cookie = _allot("cookie");
    var __query = _allot("query");
    var __life = _allot("life");
    var __popup = _allot("popup");
    var _app;
    var _route;
    var _slug;
    var _queries;

    (function () {

        _queries = __query.all();
        _defineRoute();
        _defineVueApp();

    }());

    function _defineVueApp() {

        _app = new Vue({
                data: {
                        route: _route,
                        slug: _slug,
                        publicPath: _allot("path").API,
                        lifetoken: __cookie.LIFETOKEN,
                        sign: !!(__cookie.LIFETOKEN),
                        cmsmodify: false
                    },
                beforeCreate: _beforeCreated,
                mounted: _mounted,
                methods: {
                        onLoginClick: _onLoginClick
                    },
                components: {
                        "view-header": _allot("header-directive"),
                        "view-popup": _allot("popup-directive"),
                        "view-cmsmenu": _allot("cmsmenu-directive"),
                        "view-cmsview": _allot("cmsview-directive")
                    },
                mixins: [
                        _allot("life-mixin"),
                        _allot("popup-directive-mixin"),
                        _allot("cmsview-directive-mixin"),
                        _allot("cmsprofile-directive-mixin"),
                        _allot("cmsmemberlist-directive-mixin"),
                        _allot("cmsmember-directive-mixin")
                    ]
            });
        _allot("app", _app);

    }

    function _beforeCreated() {

        _inspcetLife();

    }

    function _mounted() {



    }

    function _defineRoute() {

        var _href = window.location.href;
        window.addEventListener("hashchange", _onHashChange);
        if ( /\?$/.test(_href) ) window.location.replace(_href.replace(/\?$/, ""));
        if ( /\/cms\/$/.test(_href) ) window.location.hash = "#/";
        _updateRoute();

        function _onHashChange() {
            _href = window.location.href;
            _updateRoute();
            _inspcetLife();
            window.scrollTo(0, 0);
            _app.cmsmodify = false;
        }

        function _updateRoute() {
            var _hash = window.location.hash.replace("#/", "");
            var _pathname = window.location.pathname.replace(/\/([\w\d\-]+)(\/?[\w\d\-\%]*)*/, "$1");
            if ( _pathname === "/" ) _pathname = "home";
            else if ( /^\//.test(_pathname) ) _pathname = "essay";
            _route = _pathname + _hash ;
            switch ( _route ) {
                case "member":
                    _slug = window.location.pathname.replace(/.*\/([\w\d\-\%]+)$/, "$1");
                    _slug = decodeURI(_slug);
                    break;
                default:
                    _slug = "";
            }
            if ( /^cms/.test(_route) ) {
                _route = _route.replace(/\?.*/, "");
                if ( /^cms\w+\//.test(_route) ) {
                    _slug = _route.replace(/^\w+\/([\w\d]+)(\?.*)?/, "$1");
                    _route = _route.replace(/\/\w+/, "");
                }
            }
            if ( _app ) {
                _app.route = _route;
                _app.slug = _slug;
            }
        }

    }

    function _inspcetLife() {

        __life.session();
        __life.token(_onTokenSuccess, _onTokenError);

        function _onTokenSuccess() {
            _app.sign = true;
        }

        function _onTokenError() {
            ( _app || {} ).sign = false;
            if ( /\/cms\//.test(window.location.href) ) {
                if ( _app ) _allot("popup").alert("登入已失效", _onConfirmError);
                else _onConfirmError();
            }
        }

        function _onConfirmError() {
            window.location.replace("/");
        }

    }

    function _onLoginClick() {

        __popup.login();

    }

}(window.allot, window.Vue));