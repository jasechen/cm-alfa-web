(function (_allot) {

    "use strict";

    var __app = _allot("app");

    (function () {

        var _APP_NAME = ".app";
        __app.$mount(_APP_NAME);

    }());

}(window.allot));