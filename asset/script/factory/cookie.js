(function (_allot) {

    "use strict";

    var _cookieDictionary;

    (function () {

        _cookieDictionary = {};
        Cookie.prototype = new CookieClass();
        _allot("cookie", new Cookie());

    }());

    function Cookie() {

        var _self = this;
        Cookie.updateThis = _updateThis;
        _updateThis();

        function _updateThis() {
            var _key;
            for ( _key in _cookieDictionary ) {
                _self[_key] = _cookieDictionary[_key];
            }
        }

    }

    function CookieClass() {

        _updateDictionary();
        this.getItem = _callGetItem;
        this.addItem = _callAddItem;
        this.removeItem = _callRemoveItem;

    }

    function _updateDictionary() {

        var _cookies = document.cookie.split(/;\s?/);
        var _item;
        while ( _cookies.length ) {
            _item = _cookies.shift();
            if ( !(_item.indexOf("=")) ) continue;
            _cookieDictionary[_item.replace(/\=.+/, "")] = _item.replace(/.+\=/, "");
        }
        if ( Cookie.updateThis ) Cookie.updateThis();

    }

    function _callGetItem(_key) {

        return _cookieDictionary[_key];

    }

    function _callAddItem(_key, _value) {

        document.cookie = _key + "=" + _value + ";path=/";
        _updateDictionary();

    }

    function _callRemoveItem(_key) {

        document.cookie = _key + "=;path=/;expires=Thu, 01-Jan-1970 00:00:01 GMT;" ;
        _updateDictionary();

    }

}(window.allot));