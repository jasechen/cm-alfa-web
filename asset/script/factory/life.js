(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _lifesession;
    var _lifetoken;

    (function () {

        _allot("life", {
                session: _procedureSession,
                token: _procedureToken
            });
        _allot("life-mixin", _definedData());

    }());

    function _procedureSession() {

        _lifesession = __cookie.LIFESESSION;
        _allot("session", _lifesession);
        if ( _lifesession ) _inspectSession();
        else _initSession();

    }

    function _procedureToken(_callback, _errorback) {

        _lifetoken = __cookie.LIFETOKEN;
        _inspectToken(_callback, _errorback);

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    tokenUser: {
                            id: "",
                            firstName: "",
                            lastName: "",
                            email: "",
                            phone: "",
                            gender: "",
                            birth: "",
                            address: "",
                            status: null,
                            role: ""
                        }
                };
        }

    }

    function _inspectSession() {

        __claim("GET", "session/{session_code}", {
                "{session_code}": _lifesession
            }, _onSessionSuccess, _onSessionError);

        function _onSessionSuccess(_response) {
            if ( _response.status === "expire" ) _initSession();
        }

        function _onSessionError() {
            _initSession();
        }

    }

    function _initSession() {

        __claim("POST", "session/init", null, _onSessionInitSuccess);

        function _onSessionInitSuccess(_response) {
            var _session = _response.session;
            _allot("session", _session);
            __cookie.addItem("LIFESESSION", _session);
            window.location.reload();
        }

    }

    function _inspectToken(_callback, _errorback) {

        if ( _lifetoken ) __claim("GET", "user/token/{token}", {
                "{token}": _lifetoken
            }, _onTokenSuccess, _onTokenError);
        else _onTokenError();

        function _onTokenSuccess(_response) {
            var _responseUser = _response.user;
            if ( !(_responseUser) ) return _onTokenError();
            _responseUser.profile = _responseUser.profile || {} ;
            _responseUser.profile.phone = _allot("command").getFullPhone(_responseUser.profile.mobile_phone);
            _allot("token", _lifetoken);
            _updateTokenUser(_responseUser);
            if ( _callback ) _callback(_responseUser);
        }

        function _onTokenError() {
            __cookie.removeItem("LIFETOKEN");
            if ( _errorback ) _errorback();
        }

        function _updateTokenUser(_responseUser) {
            __app = _allot("app");
            __app.tokenUser.id = _responseUser.id;
            __app.tokenUser.firstName = _responseUser.profile.first_name;
            __app.tokenUser.lastName = _responseUser.profile.last_name;
            __app.tokenUser.email = _responseUser.profile.email;
            __app.tokenUser.phone = _responseUser.profile.phone;
            __app.tokenUser.gender = _responseUser.profile.gender;
            __app.tokenUser.birth = _responseUser.profile.birth;
            __app.tokenUser.address = _responseUser.profile.address;
            __app.tokenUser.status = _responseUser.status;
            __app.tokenUser.role = _getRole(_responseUser.roles);
            __app.tokenUser.rolelist = _allot("command").getRoleList(_responseUser.roles);
            __app.tokenUser.emailVerified = JSON.parse( _responseUser.email_checked || false );
            __app.tokenUser.phoneVerified = JSON.parse( _responseUser.mobile_checked || false );
        }

        function _getRole(_roles) {
            var _index = _roles.length;
            var _item;
            while ( _index-- ) {
                _item = _roles[_index];
                if ( _item.type !== "general" ) continue;
                if ( _item.code === "member" ) continue;
                return _item.code;
            }
        }

    }

}(window.allot));