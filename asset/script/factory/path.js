(function (_allot) {

    "use strict";

    var _pathConfig;
    var _envArray;
    var _env;

    (function () {

        _allot("path", {});
        _defineVariable();
        _setPathProperties();

    }());

    function _defineVariable() {

        _envArray = [
                "RELEASE",
                "STAGING",
                "DEVELOP"
            ];
        _pathConfig = {
                RELEASE_HOST_PATH: "//channel.money/",
                RELEASE_API_PATH: "//api.channel.money/",
                STAGING_HOST_PATH: "//test.channel.money/",
                STAGING_API_PATH: "//test.api.channel.money/",
                STAGING_GALLERY_PATH: "//test.api.channel.money/",
                DEVELOP_HOST_PATH: "//dev.channel.money/",
                DEVELOP_API_PATH: "//dev.api.channel.money/",
                DEVELOP_GALLERY_PATH: "//dev.api.channel.money/"
            };

    }

    function _setPathProperties() {

        _env = _getInspectedEnv() || "RELEASE" ;
        _allot("path").ENV = window.env = _env;
        _allot("path").HOST = _pathConfig[ _env + "_HOST_PATH" ] || "" ;
        _allot("path").API = _pathConfig[ _env + "_API_PATH" ] || "" ;
        _allot("path").GALLERY = _pathConfig[ _env + "_GALLERY_PATH" ] || "" ;

    }

    function _getInspectedEnv() {

        var _index = _envArray.length;
        var _item;
        while ( _index-- > 0 ) {
            _item = _envArray[_index];
            if ( window.location.href.indexOf(_pathConfig[ _item + "_HOST_PATH" ]) < 0 ) continue;
            return _item;
        }

    }

}(window.allot));