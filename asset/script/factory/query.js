(function (_allot) {

    "use strict";

    var _queryDictionary;

    (function () {

        _allot("query", {
                all: _consignAll,
                addItem: _consignAdd,
                deleteItem: _consignDelete,
                compare: _consignCompare
            });

    }());

    function _consignAll() {

        _inspectURLQueries();
        return _queryDictionary;

    }

    function _consignAdd(_key, _value) {

        if ( !(_key) ) return;
        if ( !(_value) ) return;
        _inspectURLQueries();
        _queryDictionary[_key] = _value;
        _updateURLQueries();

    }

    function _consignDelete(_key) {

        if ( !(_key) ) return;
        _inspectURLQueries();
        delete _queryDictionary[_key];
        _updateURLQueries();

    }

    function _consignCompare(_targetDictionary) {

        if ( JSON.stringify(_targetDictionary) === JSON.stringify(_queryDictionary) ) return true;

    }

    function _inspectURLQueries() {

        _queryDictionary = {};
        var _digit = document.location.href.indexOf("?");
        if ( _digit < 0 ) return;
        var _str = document.location.href.slice( _digit + 1 );
        var _ary = _str.split("&");
        var _item;
        while ( _ary.length ) {
            _item = _ary.shift();
            _queryDictionary[_item.replace(/=.+/, "")] = _item.replace(/.+=/, "");
        }

    }

    function _updateURLQueries() {

        var _originURL = window.location.href.replace(/\?.*/, "");
        var _str = "";
        var _key;
        for ( _key in _queryDictionary ) {
            _str += ( _str.length ) ? "&" : "?" ;
            _str += _key + "=" + _queryDictionary[_key] ;
        }
        var _url = _originURL + _str ;
        window.history.pushState({
                path: _url
            }, "", _url);
    }

}(window.allot));