(function (_allot) {

    "use strict";

    var _depotInstance;

    (function () {

        _depotInstance = {};
        _allot("depot", _depotInstance);

    }());

}(window.allot));