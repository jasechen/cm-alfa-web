(function (_allot) {

    "use strict";

    var _commandInstance;

    (function () {

        _commandInstance = {
                getFullPhone: _getFullPhone,
                getRoleList: _getRoleList
            };
        _allot("command", _commandInstance);

    }());

    function _getFullPhone(_phone) {

        if ( _phone.slice(0, 1) === "0" ) return _phone;
        return "0" + _phone ;

    }

    function _getRoleList(_roles) {

        _roles = _roles || [] ;
        var _index = _roles.length;
        var _list = [];
        var _item;
        while ( _index-- ) {
            _item = _roles[_index];
            if ( _item.type === "general" ) _list.push(_item.code);
        }
        return _list;

    }

}(window.allot));