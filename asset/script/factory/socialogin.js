(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");
    var __app = _allot("app");
    var __popup = _allot("popup");

    var _auth2;

    (function () {

        _procedure();
        _allot("socialogin", {
                signinGoogle: _signinGoogle,
                signinFacebook: _signinFacebook
            });

    }());

    function _procedure() {

        _initGoogle();
        _initFacebook();

    }

    function _initGoogle() {

        var _CLIENT_ID = "673281684195-7qon74j2upskp7ffqopm0u403fl96790.apps.googleusercontent.com";
        window.runGoogle = _runGoogle;

        function _runGoogle() {
            window.gapi.load("auth2", _onLoad);

        }

        function _onLoad() {
            _auth2 = window.gapi.auth2.init({
                    client_id: _CLIENT_ID,
                    scope: "email profile openid"
                });
        }

    }

    function _initFacebook() {

        window.fbAsyncInit = _fbAsyncInit;

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, "script", "facebook-jssdk"));

        function _fbAsyncInit() {
            window.FB.init({
                appId: "202993027155781",
                cookie: true,
                xfbml: true,
                version: "v2.12"
            });
        }

    }

    function _signinGoogle() {

        _auth2.signIn().then(_onSuccess);

        function _onSuccess(_googleUser) {
            var _profile = _googleUser.getBasicProfile();
            __app = _allot("app");
            __app.socialoginType = "google";
            __app.socialoginID = _profile.getId();
            __app.socialoginToken = _googleUser.getAuthResponse().access_token;
            __app.registerAccount = _profile.getEmail();
            __app.registerFirstname = _profile.getGivenName();
            __app.registerLastname = _profile.getFamilyName();
            _inspectLogin();
        }

    }

    function _signinFacebook() {

        window.FB.login(_onSuccess, {
                scope: "public_profile,email"
            });

        function _onSuccess(_response) {
            var _responseAuth = _response.authResponse;
            __app = _allot("app");
            __app.socialoginType = "facebook";
            __app.socialoginID = _responseAuth.userID;
            __app.socialoginToken = _responseAuth.accessToken;
            _inspectLogin();
        }

    }

    function _inspectLogin() {

        __claim("POST", "login/sns", {
                "type": __app.socialoginType,
                "sns_id": __app.socialoginID,
                "sns_token": __app.socialoginToken
            }, _onSuccess, _onError);

        function _onSuccess(_response) {
            __app.lifetoken = _response.token;
            __cookie.addItem("LIFETOKEN", __app.lifetoken);
            __popup.methods.popupLoginConfirm();
            __popup.close("login");
            window.location.reload();
        }

        function _onError(_response) {
            if ( _response.comment === "user status error" ) {
                __app.lifetoken = ( _response.data || {} ).token;
                __cookie.addItem("LIFETOKEN", __app.lifetoken);
                __popup.alert("帳號尚未驗證完成", _gotoVerify);
            } else {
                __popup.alert("繼續前確認會員資料", _onErrorConfirm);
            }
        }

        function _onErrorConfirm() {
            __popup.close("alert");
            __popup.register(null, _onErrorCancel);
        }

        function _onErrorCancel() {
            __app = _allot("app");
            __app.socialoginType = "";
            __app.socialoginID = "";
            __app.socialoginToken = "";
            __app.registerAccount = "";
            __app.registerFirstname = "";
            __app.registerLastname = "";
        }

        function _gotoVerify() {
            window.location.href = "/cms/";
        }

    }

}(window.allot));