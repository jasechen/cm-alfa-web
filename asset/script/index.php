<?php

    require "../lib/minpacker/app.php";

    header_remove();
    $etc_js_files = array(
            "allot.js",
            "factory/path.js",
            "factory/query.js",
            "factory/cookie.js",
            "factory/claim.js",
            "factory/life.js",
            "factory/popup.js",
            "factory/command.js",
            "factory/socialogin.js",
            "directive/header.js",
            "directive/popupview.js",
            "directive/cmsmenu.js",
            "directive/cmsview.js",
            "directive/cmsprofile.js",
            "directive/cmsmemberlist.js",
            "directive/cmsmember.js",
            "main.js",
            "boot.js"
        );
    header("content-type: text/javascript");
    echo Minpacker::js($etc_js_files, true);
    exit();

?>